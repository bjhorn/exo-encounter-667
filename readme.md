# EXO_encounter 667

EXO_encounter 667 is a game of exploration. You play as an unmanned
expedition to the remote exoplanet
[Gliese 667 Cc](https://en.wikipedia.org/wiki/Gliese_667_Cc) which
crashes near ruins of unknown origin. What mysteries will you find?

Written in the [Fennel programming language](https://fennel-lang.org);
winner of the [Lisp Game Jam 2018](https://itch.io/jam/lisp-game-jam-2018/results).

During the game you can press **escape** to view the help screen which lists controls.

## Install

From source: install [LÖVE](https://love2d.org) version 11.1 and run `make` in
this directory.

Downloads for Windows, Macs and GNU/Linux are [available on itch.io](https://technomancy.itch.io/exo-encounter-667).

## Development

I wrote up a series of blog posts about the development process:

* [in which a game jam is recounted](https://technomancy.us/187)
* [in which a game jam is recounted further](https://technomancy.us/188)
* [in which interactive development saves the day](https://technomancy.us/189)

This
[post by Charl Botha](https://vxlabs.com/2018/05/18/interactive-programming-with-fennel-lua-lisp-emacs-and-lisp-game-jam-winner-exo_encounter-667/)
describes how to get things installed in a way that allows you to hack
on the game source live with interactive reloading.

## Licenses

Original code, prose, map, and images copyright © 2018 [Dan Larkin](https://danlarkin.org), [Phil Hagelberg](https://technomancy.us), Zach Hagelberg, and Noah Hagelberg.

Distributed under the GNU General Public License version 3 or later; see file license.txt.

Licensing of third-party art and libraries described in [credits](credits.md)
