# EXO_ENCOUNTER.667

[Intro text as we pan over an image of a planet]

MISSION:  initial unmanned expedition to Gliese 667, 6.8 parsecs from Sol

WARNING:  radiation storm approaching; extreme magnitude

CRITICAL: detected failure in lander engines

STATUS:   lander crash; 352 meters southwest of target landing site

[Show opening gameplay scene; probe deployed next to wreckage]

Commencing self-diagnostic of probe systems...

Reactor:            [ WARN ]
Sensors:            [  ok  ]
Primary locomotion: [ FAIL ]
Rover 1:            [  ok  ]
Rover 2:            [  ok  ]
Rover 3:            [  ok  ]
Rover 4:            [  ok  ]
Comm laser:         [  ok  ]
Orbiter uplink:     [ FAIL ]

Enabling secondary rover-based locomotion.

Objective: investigate signs of intelligence.

## Gameplay

You play as an unmanned probe exploring the ruins of an unknown
civilization on an exoplanet. The ruined outpost was created as part
of an expedition to contact humanity, but it had to be abandoned when
budget cuts necessitated their withdrawal from that system.

Your probe consists of a main probe and four detachable rovers. The
main unit can only move when at least 3 of the 4 rovers are
docked. The rovers can push small rocks and other objects out of the
way. The main unit also has a powerful communications laser which can
be used to activate certain circuits within the base.

Since you land outside the base; getting to the base serves as a
tutorial where you can learn how to operate the rovers.

## Controls

The numbers 0-4 change control focus to the different rovers or the
probe, as does tab and shift+tab. When an undocked rover is selected,
the arrow keys are turn and forward, and the down arrow docks with the
probe if it's in range. When the main probe is selected, the arrows
are up/down/left/right movement, provided enough rovers are docked.

The , and . keys rotate the laser or mirror, depending on what's selected.

No mouse control; keyboard only.

## Story

The aliens abandoned this outpost in 1999, which meant that light from
1976 was just reaching them. As the Cold War was going full swing,
they developed a plan with redundant bases involving first contact
with either the USA or Russia. Your probe lands just outside the base
of the Russian contact team, so when you get inside all the text is in
Russian.

Eventually you find your way to the US-contact base and figure out
what's going on; this allows you to reactivate the outpost's AI
systems. At that point, it sends you on a mission to route power from
the Russian base to the US one, but by that point one of your rovers
has been incapacitated, so you have to make it with just three.

Once you get power restored the base comes online and contacts Earth
and the alien home world; you win.

All the Russian text can be google-translated (we don't actually want
the translation to be very good) and the English text should also be mangled.

## Timing

* outpost constructed 1956 (-3024.55 orbits)
* outpost abandoned 1999 (-2463.75 orbits)
* expedition launched on 2035
* arrived 2188 (153 year trip @ 0.15c)
 * trip took 1994 orbits
* signal arrived in Sol 23 years after the game: 2211

## Base story

* landing, constructing base
* calibrating radio telescopes, preparing probes

* prefect k'teq meli second class (459)
* science adjunct satik
* prefect nijam b'kol first class (105)
* linguistics commissioner pak megal (341)

## Skill Progression

* moving rovers
* moving probe
* pushing rocks

* firing laser
* rotating mirrors
* opening doors
* opening doors using moving rovers
* pushing boxes
