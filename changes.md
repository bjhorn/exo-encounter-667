# Changelog

## 1.0.0 / 08-05-2018

* Slow down turn rate of rovers and laser aiming.
* Allow rovers to move backwards.
* Fix tutorial progression criteria.
* Fix bug where closing doors would push probes and rovers out.
* Add sound effects.
* Animate opening and closing of doors.
* Fix bugs with scrolling.
* Speed up scrolling when selected unit is offscreen.
* Add numbers to unselected rovers.

## Game Jam Edition / 29-04-2018

* First release!
